#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_INIT(Guile-Avahi, 0.4.1, guile-avahi-bugs@nongnu.org)
AC_CONFIG_AUX_DIR(build-aux)
AC_CONFIG_MACRO_DIR(m4)

AM_INIT_AUTOMAKE([gnu check-news -Wall -Wno-portability color-tests subdir-objects])

dnl Enable silent rules by default.
AM_SILENT_RULES([yes])

AC_CONFIG_SRCDIR([src/common.c])
AC_CONFIG_HEADER([src/config.h])

dnl We require guile.m4, from Guile.  Make sure it's available.
m4_pattern_forbid([^GUILE_P])
m4_pattern_allow([^GUILE_PKG_ERRORS])

AC_ARG_WITH([guilemoduledir],
  [use the specified installation path for Guile modules],
  [case "x$withval" in
     xyes|xno) guilemoduledir="";;
     *)        guilemoduledir="$withval";;
   esac],
  [guilemoduledir=""])
   
# Libtool.
LT_PREREQ([2.2.6])
LT_INIT([disable-static dlopen])

# Checks for programs.
AC_PROG_CC
AC_LANG_C

if test "x$GCC" = "xyes"; then
  # Enable useful GCC compilation flags.
  GCC_CFLAGS="-Wall -Werror"
  GCC_CFLAGS="-Wcast-align -Wpointer-arith $GCC_CFLAGS"
  GCC_CFLAGS="-Wdeclaration-after-statement $GCC_CFLAGS"
else
  GCC_CFLAGS=""
fi
AC_SUBST([GCC_CFLAGS])


GUILE_PKG([3.0 2.2 2.0])
GUILE_PROGS
AC_PATH_PROG([guile_snarf], [guile-snarf], [not-found])
if test "x$guile_snarf" = "xnot-found"; then
   AC_MSG_ERROR([`guile-snarf' not found.  Please install Guile 2.x, 3.x or later.])
fi
GUILE_SITE_DIR

GUILE_CFLAGS="`$PKG_CONFIG guile-$GUILE_EFFECTIVE_VERSION --cflags`"
GUILE_LDFLAGS="`$PKG_CONFIG guile-$GUILE_EFFECTIVE_VERSION --libs`"
AC_SUBST([GUILE_CFLAGS])
AC_SUBST([GUILE_LDFLAGS])

AC_CACHE_SAVE


# Avahi.
AC_MSG_CHECKING([whether Avahi is available])
PKG_CHECK_MODULES([AVAHI], [avahi-client], [],
   [AC_MSG_ERROR([Avahi could not be found, please install it first.])])
AVAHI_CPPFLAGS="`$PKG_CONFIG avahi-client --cflags-only-I`"
AC_SUBST([AVAHI_CPPFLAGS])

# Checks for header files.
AC_CHECK_HEADERS([net/if.h gmp.h])
AC_CHECK_HEADER([avahi-common/thread-watch.h], [:],
   [AC_MSG_ERROR([Avahi >= 0.6.4 (with `AvahiThreadedPoll') is needed.])])

# `AVAHI_ERR_NO_CHANGE' was added in Avahi 0.6.13 (svn rev. 1266).
old_CFLAGS="$CFLAGS"
CFLAGS="$AVAHI_CFLAGS $CFLAGS"
GA_CHECK_ENUM_VALUE([#include <avahi-common/error.h>], [AVAHI_ERR_NO_CHANGE],
  [have_err_no_change=yes], [have_err_no_change=no])
AM_CONDITIONAL([HAVE_AVAHI_ERR_NO_CHANGE], test "x$have_err_no_change" = "xyes")
CFLAGS="$old_CFLAGS"

# `avahi_client_set_host_name ()' was added in Avahi 0.6.13.
old_LIBS="$LIBS"
LIBS="$AVAHI_LIBS $LIBS"
AC_CHECK_FUNCS([avahi_client_set_host_name])
LIBS="$old_LIBS"

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.
AC_CHECK_FUNCS([if_nametoindex if_indextoname])
AC_CHECK_LIB([gmp], [__gmpz_init], [LIBS="$LIBS -lgmp"],
  [AC_MSG_WARN([GNU MP not found, see README])])

AM_CONDITIONAL([CROSS_COMPILING], [test "x$cross_compiling" = "xyes"])

if test "x$guilemoduledir" = "x"; then
  guilemoduledir="${datarootdir}/guile/site/$GUILE_EFFECTIVE_VERSION"
fi
AC_SUBST([guilemoduledir])

if test "$guilemoduledir" != "$GUILE_SITE"; then
   # Guile won't be able to locate the module "out of the box", so
   # warn the user.
   AC_MSG_WARN([`guilemoduledir' ($guilemoduledir) is different from `GUILE_SITE' ($GUILE_SITE).])
   AC_MSG_WARN([Make sure to adjust the `GUILE_LOAD_PATH' environment variable accordingly,])
   AC_MSG_WARN([or re-run `configure' with `--with-guilemoduledir=$GUILE_SITE'.])
fi

# Location of the dlopenable .so.
guileextensiondir='${libdir}/guile/${GUILE_EFFECTIVE_VERSION}/extensions'
AC_SUBST([guileextensiondir])

# Do we need to hard-code $guileextensiondir in gnutls.scm?
# This is not necessary when $guileextensiondir is equal to
# Guile's 'extensiondir' as specified in 'guile-MAJOR.MINOR.pc'.
maybe_guileextensiondir="\"$guileextensiondir\""
if test "$guileextensiondir" = "`$PKG_CONFIG guile-$GUILE_EFFECTIVE_VERSION --variable extensiondir`" \
     || test "$guileextensiondir" = '$(GUILE_EXTENSION)'; then
  maybe_guileextensiondir='#f'
fi
AC_SUBST([maybe_guileextensiondir])

AC_CONFIG_FILES([Makefile
		 pre-inst-guile])
AC_CONFIG_COMMANDS([pre-inst-guile-exec], [chmod +x pre-inst-guile])

AC_OUTPUT

